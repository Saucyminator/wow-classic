# Struktur
**Läs här först:** https://jekyllrb.com/docs/structure/

- **/_data**
  - https://jekyllrb.com/docs/datafiles/
  - Data storage! Man kan ha massvis av data (t.ex: JSON, YML, etc) som man sedan kan använda vart som helst i projektet.
- **/_includes**
  - https://jekyllrb.com/docs/includes/
  - Lite kod som man kan inkludera var som helst i projektet. T.ex: head.html och scripts.html används för att sätta upp `<head>` och `<script>` taggarna på sidan.
- **/_layouts**
  - https://jekyllrb.com/docs/layouts/
  - Kan sätta upp hur en sida ska visas. page.html används för att visa sidorna. Behöver oftast inte ändra mycket här utan görs mest i sidorna själv (mer om det strax).
- **/_posts**
  - https://jekyllrb.com/docs/posts/
  - Används för blogposts. Man kan även ha _posts i en undermapp, undermappens namn blir kategorinamnet som man kan använda för att sortera.
- **/_sass**
  - https://jekyllrb.com/docs/assets/#sassscss
  - CSS/SASS stuff. _main.scss används mest för att styla, de andra är mest för setup.
- **pages (/404, /priest, /test, etc)**
  - https://jekyllrb.com/docs/pages/
  - En sida som visas på hemsidan. Undermapp med index.html med lite jekyll settings överst i filen gör så att den syns i browsern.
    - layout - används för vilken layout sidan ska användas (nästan alltid "page")
    - title - titeln på sidan
    - nav - om sidan ska visas i navbar (production)
    - dev - om sidan ska visas i navbar (development)
- **/src**
  - Stället där CSS, JS, misc förvaras. Använder /src att spara bilder.
    - I CSS mappen finns main.scss som referencear filerna i /_sass. Ändra inget här i utan gör det i /_sass.
    - I IMAGES lägger jag alla bilder.
    - I JS mappen finns main.js. Go wild. Är referenced i /_includes/scripts.html
- **/vendor**
  - Ska inte in i repot, var Jekyll sparar alla libraries som används för att det ska funka från **Gemfile**.
- **_config.yml**
  - Settings-fil för hela projektet. Används mest för Jekyll inställningar men kan lägga till själv. **_OBS! När man ändrar något i denna filen medans man har Jekyll Serve igång så måste man starta om Jekyll Serve!_**
- **.gitlab-ci.yml**
  - GitLab fil som lägger upp projektet på GitLab pages.
- **Gemfile**
  - Jekyll installations fil.
- **index.html**
  - Startsidan.

---

1. Installera jekyll: https://jekyllrb.com/docs/installation/
2. `bundle install`
3. `bundle exec jekyll serve`
